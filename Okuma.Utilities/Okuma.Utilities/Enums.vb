﻿Imports System.ComponentModel

Public Enum AlarmLevelEnum As Integer
    <Description("Alarm B")> _
    B = 3
    <Description("Alarm C")> _
    C = 2
    <Description("Alarm D")> _
    D = 1
End Enum

Public Enum MachineTypeEnum As Integer
    Lathe = 0
    Mill = 1
    Sim = 2
    S = 3
    Grinder = 4
End Enum

Public Enum ControlTypeEnum As Integer
    P100 = 100
    P200 = 200
    P300 = 300
End Enum

Public Enum APIProblemEnum
    Offline = 0
    NoAPI = 1
    OldAPI = 2
    Exception = 3
    NoError = 4
End Enum

Public Enum AppTypeEnum As Integer
    Process = 0
    Service = 1
End Enum

Public Enum LauchTypeEnum As Integer
    LaunchOnce = 0
    Monitor = 1
End Enum

Public Enum WaitForOspReturnEnum
    OspStarted = 0
    SimulationMode = 1
    Timeout = 2
End Enum
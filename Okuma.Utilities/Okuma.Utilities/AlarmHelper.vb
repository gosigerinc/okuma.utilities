﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Text

''' <summary>
''' Helper class for modifying the Okuma Pocket Manual description for User alarms
''' </summary>
''' <remarks></remarks>
Public Class AlarmHelper
    Public Shared Sub WriteAlarmHelp(AlarmLevel As AlarmLevelEnum, AlarmString As String, Message As String, Optional [Object] As String = "", Optional CharacterString As String = "", Optional Code As String = "", Optional FaultyLocation As String = "")

        Dim machineCode = If(DMC.GetMachineType = MachineTypeEnum.Mill, "M", "L")
        Dim alarmCode = [Enum].GetName(GetType(AlarmLevelEnum), AlarmLevel)

        Dim alarmNumberStart As Integer

        Select Case AlarmLevel
            Case AlarmLevelEnum.C
                alarmNumberStart = 3
            Case AlarmLevelEnum.D
                alarmNumberStart = 4
            Case Else
                alarmNumberStart = 2
        End Select

        Dim alarmFilePath = Path.Combine(String.Format("C:\OSP-P\P-MANUAL\{0}PA\ENG", machineCode), String.Format("ALARM-{0}.HTM", alarmCode))

        Dim outputBuff As New StringBuilder
        Dim buff As String = ""
        Dim lineSpace = "       "
        Dim lineEnd = "<!--**-->"
        Using sr As New StreamReader(alarmFilePath)
            While Not sr.EndOfStream
                buff = sr.ReadLine
                If buff.Contains(String.Format("{0}127</FONT>", alarmNumberStart)) Then
                    outputBuff.AppendLine(buff.Replace("THiNC Alarm", String.Format("<!--**AlarmString-->{0}<!--AlarmString**-->", AlarmString)))
                    'We found the first line
                    'Skip the next 3 lines
                    outputBuff.AppendLine(sr.ReadLine)
                    outputBuff.AppendLine(sr.ReadLine)
                    outputBuff.AppendLine(sr.ReadLine)
                    'Dump Message line
                    sr.ReadLine()
                    outputBuff.AppendLineFormat("<!--**Message-->{0}{1}{2}", lineSpace, Message, lineEnd)
                    'Skip the next line
                    outputBuff.AppendLine(sr.ReadLine)
                    'Dump Object Line
                    sr.ReadLine()
                    outputBuff.AppendLineFormat("<!--**Object--> {0}{1}{2}", lineSpace, [Object], lineEnd)
                    'Skip the next line
                    outputBuff.AppendLine(sr.ReadLine)
                    'Dump CharacterString Line
                    sr.ReadLine()
                    outputBuff.AppendLineFormat("<!--**CharacterString--> {0}{1}{2}", lineSpace, CharacterString, lineEnd)
                    'Skip the next line
                    outputBuff.AppendLine(sr.ReadLine)
                    'Dump Code Line
                    sr.ReadLine()
                    outputBuff.AppendLineFormat("<!--**Code--> {0}{1}{2}", lineSpace, Code, lineEnd)
                    'Skip the next line
                    outputBuff.AppendLine(sr.ReadLine)
                    'Dump FaultyLocation Line
                    sr.ReadLine()
                    outputBuff.AppendLineFormat("<!--**FaultyLocation--> {0}{1}{2}", lineSpace, FaultyLocation, lineEnd)
                Else
                    outputBuff.AppendLine(buff)
                End If
            End While
        End Using

        File.WriteAllText(alarmFilePath, outputBuff.ToString)

    End Sub

    Public Shared Sub RemoveAlarmHelp(AlarmLevel As AlarmLevelEnum)
        Dim machineCode = If(dmc.GetMachineType = MachineTypeEnum.Mill, "M", "L")
        Dim alarmCode = [Enum].GetName(GetType(AlarmLevelEnum), AlarmLevel)

        Dim alarmFilePath = Path.Combine(String.Format("C:\OSP-P\P-MANUAL\{0}PA\ENG", machineCode), String.Format("ALARM-{0}.HTM", alarmCode))

        Dim titleRemovalRegex As New Regex("\<\!\-\-\*\*AlarmString\-\-\>.*\<\!\-\-AlarmString\*\*\-\-\>",
                                           RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Compiled)

        Dim removalRegex As New Regex("\<\!\-\-\*\*\w{1,15}\-\-\>.*?\<\!\-\-\*\*\-\-\>",
                                      RegexOptions.IgnoreCase Or RegexOptions.Singleline Or RegexOptions.Compiled)

        Dim buff As String = ""
        Using sr As New StreamReader(alarmFilePath)
            buff = sr.ReadToEnd
            buff = titleRemovalRegex.Replace(buff, "THiNC Alarm")
            buff = removalRegex.Replace(buff, "       Undefined")
        End Using

        File.WriteAllText(alarmFilePath, buff)
    End Sub
End Class

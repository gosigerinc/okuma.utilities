﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Text

''' <summary>
''' Utility to pull data out of the Okuma 'Data Management Card'
''' </summary>
''' <remarks></remarks>
Public Class DMC

    Public Shared Function GetMachineType() As MachineTypeEnum

        Return GetMachineInfo.MachineType

    End Function

    Public Shared Function GetMachineInfo() As MachineInformation

        Const dmcPath As String = "c:\osp-p\ospmngcd.cnc"

        Dim rtnMachInfo As New MachineInformation

        If File.Exists(dmcPath) Then

            Try
                Dim buff As String = ""
                Dim machineTypeRegex As New Regex("OSP-(P(?:1|2|3)0{2})((?:S|L|M|Z))")
                Using sr As New StreamReader(dmcPath)
                    While Not sr.EndOfStream
                        buff = sr.ReadLine
                        If machineTypeRegex.IsMatch(buff) Then
                            Dim match = machineTypeRegex.Match(buff)
                            If match IsNot Nothing Then
                                rtnMachInfo.FullControlName = match.Groups(0).Value
                                Dim plevel = match.Groups(1)
                                rtnMachInfo.ControlType = [Enum].Parse(GetType(ControlTypeEnum), plevel.Value)
                                Dim type = match.Groups(2)
                                Select Case type.Value
                                    Case "S"
                                        'rtnMachInfo.MachineType = MachineTypeEnum.S
                                        rtnMachInfo.IsSControl = True
                                        rtnMachInfo.MachineType = MachineTypeEnum.Lathe
                                    Case "L"
                                        rtnMachInfo.MachineType = MachineTypeEnum.Lathe
                                    Case "M"
                                        rtnMachInfo.MachineType = MachineTypeEnum.Mill
                                    Case Else
                                        rtnMachInfo.MachineType = MachineTypeEnum.Sim
                                        Exit Select
                                End Select
                            End If
                            Exit While
                        End If
                    End While
                End Using


            Catch ex As Exception
                Throw New Exception("Problem reading the Data Mangment Card", ex)
            End Try
        End If

        Return rtnMachInfo

    End Function

End Class

Public Class MachineInformation

    Public Property MachineType As MachineTypeEnum = MachineTypeEnum.Sim

    Public Property ControlType As ControlTypeEnum = ControlTypeEnum.P200

    Public Property FullControlName As String = "OSP-P200L"

    Public Property IsSControl As Boolean = False

    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder
        sb.AppendLine("Machine type: " & MachineType)
        sb.AppendLine("Control type: " & ControlType)
        sb.AppendLine("Full Control Name: " & FullControlName)
        sb.AppendLine("Is 'S' control?: " & IsSControl)
        Return sb.ToString
    End Function

End Class

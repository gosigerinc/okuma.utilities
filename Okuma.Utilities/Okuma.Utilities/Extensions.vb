﻿Imports System.Runtime.CompilerServices
Imports System.Text

Module Extensions

    <Extension()>
    Public Function AppendLineFormat(ByVal builder As StringBuilder, ByVal format As String, ByVal ParamArray args As Object()) As StringBuilder
        Dim value As String = String.Format(format, args)

        builder.AppendLine(value)

        Return builder
    End Function

    ''' <summary>
    ''' Attempts to modify name of a .MIN program so that it can go into the MD1 folder on the Okuma
    ''' </summary>
    ''' <param name="s"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function ToCleanOkumaProgramName(s As String) As String
        Dim rtn As String = ""
        rtn = s.Replace("_", "-").Replace(" ", "").ToUpper()
        If IsNumeric(rtn.Substring(0, 1)) Then rtn = rtn.Insert(0, "A")
        If Not rtn.EndsWith(".MIN") Then
            rtn += ".MIN"
        End If
        If rtn.Length > 15 Then
            rtn = rtn.Substring(0, 10) & ".MIN"
        Else
            rtn = rtn
        End If

        Return rtn

    End Function

End Module

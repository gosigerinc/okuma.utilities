﻿Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.IO

''' <summary>
''' Check the version of the API and if it is running
''' </summary>
''' <remarks></remarks>
Public Class API
    ''' <summary>
    ''' Returns the Version of the THINC API; Returns nothing if THINC API is not installed
    ''' </summary>
    Public Shared Function GetTHiNCAPIVersion() As Version
        Dim _return As Version = Nothing
        Const _uninstallkey As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
        Dim rk As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(_uninstallkey)
        Dim sk As Microsoft.Win32.RegistryKey
        Dim skname() = rk.GetSubKeyNames
        For counter As Integer = 0 To skname.Length - 1
            sk = rk.OpenSubKey(skname(counter))
            If sk.GetValue("DisplayName") <> "" Then
                If sk.GetValue("DisplayName").ToString.ToUpper.Contains("THINC-API") Then
                    Dim dv As String = sk.GetValue("DisplayVersion")
                    If dv IsNot Nothing Then
                        _return = New Version(dv)
                    End If
                End If
            End If
        Next
        Return _return
    End Function

    ''' <summary>
    ''' Checks the currently installed API version for a version >= 1.9.1
    ''' </summary>
    ''' <param name="MachineType">Type of machine you are checking</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAPI() As APIInializeException
        Return CheckAPI("1.9.1")
    End Function

    ''' <summary>
    ''' Checks the installed API version against a required minimum version number
    ''' </summary>
    ''' <param name="RequiredAPIVersion">Version of the API required. Enter like 1.1.1 or 12.12.12</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAPI(RequiredAPIVersion As String) As APIInializeException
        Dim rtn As New APIInializeException("", APIProblemEnum.NoError)

        Dim versionRegex As New Regex("\d{1,2}\.\d{1,2}\.\d{1,2}")
        Dim version As Version

        If versionRegex.IsMatch(RequiredAPIVersion) Then
            'Use Regex to clean the string to xx.xx.xx format i.e. strip out 'V' characters
            version = New Version(versionRegex.Match(RequiredAPIVersion).Value)
        Else
            Return New APIInializeException(String.Format("The version you specified: {0} is not a valid version format. Reformat the version like this: 1.1.1 or 22.22.22", RequiredAPIVersion), APIProblemEnum.Exception)
        End If
        Dim machineType = DMC.GetMachineType
        If machineType = MachineTypeEnum.Lathe OrElse machineType = MachineTypeEnum.Mill Then
            Dim aPIVersion = GetTHiNCAPIVersion()
            If aPIVersion Is Nothing Then
                rtn = New APIInializeException("API not installed on this machine", APIProblemEnum.NoAPI)
            End If
            'Compare the version number to the required version number .CompareTo returns -1 if older, 0 if same and 1 if newer
            Dim vc = aPIVersion.CompareTo(version)
            If vc = -1 Then
                rtn = New APIInializeException(String.Format("API version {0} or greater required", RequiredAPIVersion), APIProblemEnum.OldAPI)
            End If
        End If
        Return rtn
    End Function

    Public Shared Function WaitForOspStart(Timeout As TimeSpan) As WaitForOspReturnEnum
        If File.Exists("C:\OSP-P\OSPMNGCD.CNC") Then
            Dim startTime = Now
            Dim myProcess As Process() = Process.GetProcessesByName("PNC-P200")
            While myProcess.Length = 0
                If Now.Subtract(startTime) >= Timeout Then Return WaitForOspReturnEnum.Timeout
                myProcess = Process.GetProcessesByName("PNC-P200")
                Thread.Sleep(1000)
            End While
            Return WaitForOspReturnEnum.OspStarted
        End If
        Return WaitForOspReturnEnum.SimulationMode
    End Function

End Class

Public Class APIInializeException
    Inherits Exception

    Public ReadOnly APIProblem As APIProblemEnum

    Public Sub New(ByVal Message As String, APIProblem As APIProblemEnum)
        Me.New(Message, APIProblem, Nothing)
    End Sub

    Public Sub New(ByVal Message As String, APIProblem As APIProblemEnum, InnerExecption As Exception)
        MyBase.New(Message, InnerExecption)
        Me.APIProblem = APIProblem
    End Sub
End Class



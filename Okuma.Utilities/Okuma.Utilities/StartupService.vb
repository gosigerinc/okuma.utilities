﻿Imports Microsoft.Win32

Public Class StartupService

    Public Overloads Shared Sub Register(ApplicationPath As String, ApplicationName As String, AppType As AppTypeEnum, wait As Boolean, LaunchType As LauchTypeEnum)

        Dim ObjReg As New CReg
        Dim regCreated As Boolean

        If ObjReg.ReadValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\StartupService\", "State") Then

            'Startup Service is installed

            If Not ObjReg.ReadValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName, "Enabled") Then
                'No entry for this program
                regCreated = ObjReg.CreateSubKey(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName)

            Else
                regCreated = True
            End If

            If regCreated Then
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                 "Type", "Process")
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                  "Name", ApplicationName)
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                  "Enabled", "True")
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                  "Wait", If(wait, "True", "False"))
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                  "Type", If(AppType = AppTypeEnum.Process, "Process", "Service"))
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                  "Launch", If(LaunchType = LauchTypeEnum.LaunchOnce, "Once", "Monitor"))
                ObjReg.WriteValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & ApplicationName,
                                 "File", ApplicationPath)
            End If


        End If

    End Sub

    Public Overloads Shared Sub Register(ThisAssembly As System.Reflection.Assembly, ByVal AppType As AppTypeEnum, ByVal Wait As Boolean, ByVal LaunchType As LauchTypeEnum)
        Dim AppName = ThisAssembly.FullName.Split(",")(0)
        Dim AppPath = ThisAssembly.Location
        Register(AppPath, AppName, AppType, Wait, LaunchType)
    End Sub

    Public Shared Sub UnRegister()

        Dim ObjReg As New CReg
        Dim AppName = System.Reflection.Assembly.GetExecutingAssembly().FullName.Split(",")(0)

        If ObjReg.ReadValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\StartupService\", "State") Then
            If ObjReg.ReadValue(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & AppName, "Enabled") Then
                ObjReg.DeleteSubKey(ObjReg.HKeyLocalMachine, "SOFTWARE\OAC\Startup\" & AppName)
            End If

        End If

    End Sub


End Class
